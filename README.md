2/27/19
Main Java class for 2019 FRC Robot

2 joysticks:
	Extreme 3D Pro Joystick
	Logitech Gamepad F310
	
Drivetrain: Mecanum (4 CIM Motors)
Arm: 6 bar lift (2 CIM Motors)
Mechanism: Intake (1 775 Motor)

Pneuatics: 1 double acting solenoid
	x2 2" stroke pistons
	
Camera: Microsoft LifeCam HD-3000

PID Controller:
	analog 270 degree potentiometer
	
Robot Controlls: 
